<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Test Blog</title>
  <!-- Bootstrap core CSS -->
  <link href="http://localhost/training2/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template -->
  <link href="http://localhost/training2/css/all.min.css" rel="stylesheet" type="text/css">
  <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
  <!-- Custom styles for this template -->
  <link href="http://localhost/training2/css/clean-blog.min.css" rel="stylesheet">

</head>

<body>

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link" href="http://localhost/training2/main.php">Home</a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $id; ?>/edit">Edit Blog</a>  
          </li>
          <li class="nav-item">
            <a class="nav-link" href="<?php echo $id; ?>/delete" onclick="return confirm('Are you sure you want to delete this blog?')">Delete Blog</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('http://localhost/training2/img/post-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="post-heading">
              <?php
                  echo '<h1>'.$row["Title"].'</h1>
                        <span class="meta">Posted on '.$row["Date"].'</span>
                        <div class="container">
                        <div class="row">'; 
              ?> 
          </div>
        </div>
      </div>
    </div>
  </header>

  <!--Post Content -->
  <<article>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <p><?php echo $row["Content"]; ?></p> 
          <?php
          $data2 = fetchTag($conn, $id);
          echo "<p>Tags: ";
          if (isset($data2)) { 
            foreach ($data2 as $row2) {
              echo '<a href="tagpost.php?tag='.$row2["tid"].'">'.$row2["tname"].' </a>';            
            }
          echo "</p>";
          }
          echo "<p>Categories: ";
          $dat = fetchCat($conn, $id);
          if (isset($dat)) {
            foreach ($dat as $row2) {
              $tagidval = $row2["cid"];
              echo $row2["cname"]." ";
            }
          echo "</p>";
          }  
          ?>
        </div>
      </div>
    </div>
  </article>
  <hr>
  <!-- Bootstrap core JavaScript -->
  <script src="http://localhost/training2/s/jquery.min.js"></script>
  <script src="http://localhost/training2/js/bootstrap.bundle.min.js"></script>
  <!-- Custom scripts for this template -->
  <script src="http://localhost/training2/js/clean-blog.min.js"></script>
</body>

</html>
