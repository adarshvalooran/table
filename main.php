<?php
require 'model/blog.php';
$uri = $_SERVER['REQUEST_URI'];
$uri = explode('/', $uri);
if (!is_file('config.php')) {
  require 'view/install.php';
  if (isset($_POST['submit'])) {
    $db = $_POST['db'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    if ($username == "root" and $password == "user0123") {
      createFile($username, $password, $db);
      require 'config.php';
      try {
        $conn = new PDO("mysql:host=$hostname", $username, $password);
        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        dropDb($conn, $dbname);
        createDb($conn, $dbname);
        createTable($conn);
        $dummyNo = $_POST['dummyNo']; 
        while ($dummyNo != 0)  {
          insertDummy($conn);
          $dummyNo--;
        }
      }
      catch(PDOException $e) {
        $conn->rollback();
        echo "Error: " . $e->getMessage();
      }
      header("Location:main.php");
    } else {
      echo "<script type='text/javascript'> alert('Username or Password is incorrect');</script>";
    }
  }
}
if ($uri[3] == 'post') {
  $conn = db_get_connection();
  $id = $uri[4];
  if ($uri[5] == 'delete'){
    delBlog($id, $conn);
    deleteCatRel($conn, $id);
    header("Location:http://localhost/training2/main.php");
  }
  if ($uri[5] == 'edit'){
    $row = fetchBlog($conn, $id);
    $data2 = fetchTag($conn, $id);  
    $data = fetchAllCat($conn);
    
    require 'view/add.php';
  //   if (isset($_POST['title'])) {
  //     $date = date("Y-m-d H:i:s");
  //     $title = $_POST['title'];
  //     $blog = $_POST['content'];
  //     $tag = $_POST['tags'];
  //     $words = trimInput($tag);
  //     $s = sizeof($words);
  //     updateBlog($id, $title, $blog, $date, $conn);
  //     del($id, $conn);
  //     for ($i = 0; $i < $s; $i++) {
  //       $count = tagCount($conn, $words, $i);
  //       if ($count == 0) {
  //         insertTag($conn, $words, $i);
  //       }
  //       insertRel($conn, $words, $title, $i);
  //     }
  //     delTab($conn);
  //     deleteCatRel($conn, $id);
  //     foreach($_POST['list'] as $s) {
  //       $sql = "SELECT cid FROM CatTable WHERE cname = '$s'";
  //       $q = $conn->query($sql);
  //       $q->setFetchMode(PDO::FETCH_ASSOC);
  //       $cid = $q->fetch();
  //       insertCatRel($conn, $id, $cid['cid'], $title, $s);
  //   }
  //     header("Location:http://localhost/training2/main.php");
  //   }
  // }
    $flag = 1;
  }
    $row = fetchBlog($conn, $id);
    require 'view/post.php'; 
  if ($uri[3] == 'add') {
    $conn = db_get_connection();
    if (isset($_POST['submit'])) {
      $title = $_POST['title'];
      $blog = $_POST['content'];
      $tag = $_POST['tags'];
      $date = date("Y-m-d H:i:s");
      $words = trimInput($tag);
      $s = sizeof($words);
      if ($flag == 1) {
        updateBlog($id, $title, $blog, $date, $conn);
        del($id, $conn);
      } else {
        insertBlog($title, $blog, $date, $conn);
      } 
      for ($i = 0; $i < $s; $i++) { 
        $count = tagCount($conn, $words, $i);
        if ($count == 0) {
          insertTag($conn, $words, $i);
        }   
        insertRel($conn, $words, $title, $i);
      }
    // $sq = "SELECT id FROM BlogDetails WHERE Title = '$title'";
    // $q = $conn->query($sq);
    // $q->setFetchMode(PDO::FETCH_ASSOC);
    // $id = $q->fetch();
      if ($flag == 1) {
        delTab($conn);
        deleteCatRel($conn, $id);
      }
      foreach($_POST['list'] as $s) {
        $sql = "SELECT cid FROM CatTable WHERE cname = '$s'";
        $q = $conn->query($sql);
        $q->setFetchMode(PDO::FETCH_ASSOC);
        $cid = $q->fetch();
        insertCatRel($conn, $id['id'], $cid['cid'], $title, $s);
      }
      header("Location:http://localhost/training2/main.php");
    }  
    $data = fetchAllCat($conn);
    require 'view/add.php';
  }
}  else {
  $conn = db_get_connection();
  if (is_numeric($uri[3])) {
    $pageno = $uri[3];
  } else {
    $pageno = 1;
  }
  if ($uri[4] == 'ASC') {
    $sort = ASC;
  } else {
    $sort = DESC;
  }
  $n = 3;
  $offset = ($pageno-1) * $n;
  $total_pages = page($conn, $n);
  $d = sortDisp($conn, $offset, $sort, $n);
  require 'view/list.php';
}

?>