<?php
function generate_table($header, $rows) {
  $hc = count($header);
  $rc = count($rows[0]);
  $rr = count($rows);
  if ($hc == 0 and $rc == 0) {
    return '';
  }
  $htm = '<!DOCTYPE html>
  <html>
    <head>
      <title>HTML Table Cellpadding</title>
     </head>
     <body>
     <table border = "1" cellpadding = "2" cellspacing = "5" ';
    if ($hc == 0 and $rr !=0) {
      $htm .= 'class="tableAlt2">';
    } else {
      $htm .= 'class="tableAlt1">';
    }
  if ($hc != 0) {
    $htm .= '<tr bgcolor="#71f584">';
    foreach ($header as $h) {
      $htm .= '<th>'.$h.'</th>';
    }
    if ($hc != 0 and $rr ==0) {
      $htm .= '</tr>';
      $htm .= '</table>
        </body>
        </html>';
      return $htm;
    }
    
  }
  if ($rr != 0) {
    for ($i = 0; $i < $rr; $i++) {
      $htm .= '<tr>';
      for ($j = 0; $j < $rc; $j++) {
        $htm .= '<td>'.$rows[$i][$j].'</td>';
      }
    }
    if ($hc == 0 and $rr !=0) {
      $htm .= '<tr>';
      $htm .= '</table>
        </body>
        </html>';
      return $htm;
    }  
  }
  $htm .= '<tr>';
  $htm .= '</table>
    </body>
    </html>';
  if ($hc != $rc) {
    return '';
  }
  return $htm;

}

function generate_data($a, $b) {
  for ($i = 0; $i < $a; $i++) {
    for ($j = 0; $j < $b; $j++) {
      $rows[$i][$j] = rand(1,100); 
    }
   }
  return $rows;
}
// $rows = array();
$rows = generate_data(4, 3);
$header = array("c1", "c2", "c3");
// $header = array();
echo generate_table($header, $rows);
?> 

<style type="text/css">

	.tableAlt1 tr:nth-child(even) { 
		background: #b6e9fa;
	}

  .tableAlt2 tr:nth-child(odd) { 
		background: #b6e9fa;
	}
  
</style>
